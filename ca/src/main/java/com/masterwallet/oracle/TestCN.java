package com.masterwallet.oracle;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Call;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class TestCN {

	public static void main(String[] args) throws Exception {
		String phone = "79253468737";
		String pin = "123";
		String url =  "https://gate.smsaero.ru/v2/"
				+ "sms/send?numbers[]=" + phone + "&text=CODE+" + pin + "&sign=SMS Aero&channel=DIRECT";

		 OkHttpClient client = new OkHttpClient.Builder()
			      .readTimeout(1, TimeUnit.SECONDS)
			      .authenticator(new Authenticator() {
			          @Override
			          public Request authenticate(Route route, Response response) throws IOException {
			              String credential = Credentials.basic("akafakir@gmail.com", "0YBEW6jzInyp9TNBm63e2s8dYYC");
			              return response.request().newBuilder().header("Authorization", credential).build();
			          }
			      })
			      .build();

		 Request request = new Request.Builder()
			      .url( url)
			      .build();

			    Call call = client.newCall(request);
			    Response response = call.execute();

			    System.out.println(response.body().string());
			    System.out.println(response.code());

	}

}
