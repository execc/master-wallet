package com.masterwallet.oracle.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Bytes32;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

import io.reactivex.Flowable;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.2.0.
 */
public class MasterWallet extends Contract {
    private static final String BINARY = "608060405234801561001057600080fd5b50604051608080610f688339810160409081528151602083015191830151606090930151909290600160a060020a038416151561004c57600080fd5b600160a060020a038316151561006157600080fd5b6000821161006e57600080fd5b80151561007a57600080fd5b60008054600160a060020a03958616600160a060020a0319918216179091556001805494909516931692909217909255600391909155600555610ea6806100c26000396000f3006080604052600436106100c45763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416630ba234d681146100c95780632becdd80146100e05780632ccb1b301461014b5780633804e8a91461016f57806338add1d3146101875780634941fdd0146101e05780635deca9eb146101f557806387eb94511461020a578063ad9152b514610236578063b71d2cec14610257578063c742380314610288578063d8389dc51461029d578063ded203a6146102b5575b600080fd5b3480156100d557600080fd5b506100de61031e565b005b3480156100ec57600080fd5b506040805160206004803580820135601f810184900484028501840190955284845261013994369492936024939284019190819084018382808284375094975061045a9650505050505050565b60408051918252519081900360200190f35b34801561015757600080fd5b506100de600160a060020a0360043516602435610524565b34801561017b57600080fd5b506100de600435610626565b34801561019357600080fd5b506040805160206004803580820135601f81018490048402850184019095528484526101399436949293602493928401919081908401838280828437509497506106f29650505050505050565b3480156101ec57600080fd5b506101396107f8565b34801561020157600080fd5b506101396107ff565b34801561021657600080fd5b50610222600435610820565b604080519115158252519081900360200190f35b34801561024257600080fd5b506100de600160a060020a0360043516610835565b34801561026357600080fd5b5061026c610940565b60408051600160a060020a039092168252519081900360200190f35b34801561029457600080fd5b506100de610944565b3480156102a957600080fd5b50610139600435610ab9565b3480156102c157600080fd5b50604080516020600460443581810135601f81018490048402850184019095528484526100de9482359460248035600160a060020a031695369594606494920191908190840183828082843750949750610b039650505050505050565b600054600160a060020a03163314610382576040805160e560020a62461bcd02815260206004820152602c6024820152600080516020610dfb8339815191526044820152600080516020610e5b833981519152606482015290519081900360840190fd5b600254600160a060020a0316151561039957600080fd5b6003546004540142106103f6576040805160e560020a62461bcd02815260206004820152601760248201527f43616e206e6f742063616e63656c207265636f76657279000000000000000000604482015290519081900360640190fd5b60025460008054604051600160a060020a03938416939091169130917f58171a6cc0d54283ed3199ebaca83d39f38bfe5ee69776d7511bddb58174d2b09190a46002805473ffffffffffffffffffffffffffffffffffffffff191690556000600455565b6000816040516020018082805190602001908083835b6020831061048f5780518252601f199092019160209182019101610470565b6001836020036101000a0380198251168184511680821785525050505050509050019150506040516020818303038152906040526040518082805190602001908083835b602083106104f25780518252601f1990920191602091820191016104d3565b5181516020939093036101000a6000190180199091169216919091179052604051920182900390912095945050505050565b600054600160a060020a03163314610588576040805160e560020a62461bcd02815260206004820152602c6024820152600080516020610dfb8339815191526044820152600080516020610e5b833981519152606482015290519081900360840190fd5b600254600160a060020a0316156105eb576040805160e560020a62461bcd02815260206004820152603a6024820152600080516020610e3b8339815191526044820152600080516020610e1b833981519152606482015290519081900360840190fd5b604051600160a060020a0383169082156108fc029083906000818181858888f19350505050158015610621573d6000803e3d6000fd5b505050565b600054600160a060020a0316331461068a576040805160e560020a62461bcd02815260206004820152602c6024820152600080516020610dfb8339815191526044820152600080516020610e5b833981519152606482015290519081900360840190fd5b600254600160a060020a0316156106ed576040805160e560020a62461bcd02815260206004820152603a6024820152600080516020610e3b8339815191526044820152600080516020610e1b833981519152606482015290519081900360840190fd5b600355565b6000816040516020018082805190602001908083835b602083106107275780518252601f199092019160209182019101610708565b6001836020036101000a0380198251168184511680821785525050505050509050019150506040516020818303038152906040526040518082805190602001908083835b6020831061078a5780518252601f19909201916020918201910161076b565b51815160001960209485036101000a01908116901991909116179052604080519490920184900384208482015281518085038201815293820191829052835193955090935083929085019150808383602083106104f25780518252601f1990920191602091820191016104d3565b6005545b90565b6004546000901561081a5742600354600454010390506107fc565b50600090565b600061082b82610ab9565b6005541492915050565b600054600160a060020a03163314610899576040805160e560020a62461bcd02815260206004820152602c6024820152600080516020610dfb8339815191526044820152600080516020610e5b833981519152606482015290519081900360840190fd5b600254600160a060020a0316156108fc576040805160e560020a62461bcd02815260206004820152603a6024820152600080516020610e3b8339815191526044820152600080516020610e1b833981519152606482015290519081900360840190fd5b600160a060020a038116151561091157600080fd5b6001805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a0392909216919091179055565b3090565b600254600160a060020a031633146109cc576040805160e560020a62461bcd02815260206004820152602260248201527f4f6e6c79207265616c206f776e65722063616e206d616b65207468697320636160448201527f6c6c000000000000000000000000000000000000000000000000000000000000606482015290519081900360840190fd5b600254600160a060020a031615156109e357600080fd5b600354600454014211610a40576040805160e560020a62461bcd02815260206004820152601660248201527f43616e206e6f74206d616b65207265636f766572792000000000000000000000604482015290519081900360640190fd5b60025460008054604051600160a060020a03938416939091169130917fbfe5ce1f9ee64c971633c18da96c8efba09f9b7ade08097976a896c700479c199190a4600280546000805473ffffffffffffffffffffffffffffffffffffffff19908116600160a060020a038416178255909116909155600455565b60408051602080820184905282518083038201815291830192839052815160009391829190840190808383602083106104f25780518252601f1990920191602091820191016104d3565b600154600090600160a060020a03163314610b68576040805160e560020a62461bcd02815260206004820181905260248201527f4f6e6c792064656c65676174652063616e206d616b6520746869732063616c6c604482015290519081900360640190fd5b600254600160a060020a031615610bcb576040805160e560020a62461bcd02815260206004820152603a6024820152600080516020610e3b8339815191526044820152600080516020610e1b833981519152606482015290519081900360840190fd5b610bd484610ab9565b60055414610c2c576040805160e560020a62461bcd02815260206004820152601760248201527f4964656e746974792068617368206c6f636b206661696c000000000000000000604482015290519081900360640190fd5b50604080517fe8fc9273000000000000000000000000000000000000000000000000000000008152600160a060020a038416600482019081526024820192835283516044830152835173028309c4872abd639dfbf0114db53574b678f06693849363e8fc92739388938893919260640190602085019080838360005b83811015610cc0578181015183820152602001610ca8565b50505050905090810190601f168015610ced5780820380516001836020036101000a031916815260200191505b509350505050602060405180830381600087803b158015610d0d57600080fd5b505af1158015610d21573d6000803e3d6000fd5b505050506040513d6020811015610d3757600080fd5b50511515610d8f576040805160e560020a62461bcd02815260206004820152601b60248201527f4f7261636c65207665726966696361746974696f206661696c65640000000000604482015290519081900360640190fd5b6002805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03858116919091179182905542600455600080546040519383169392169130917f984ae6bb7fc6ec202143bd903c70af8408ed9b154ef3e4cfe78e47a90ebbe2e491a45050505056004f6e6c79207265616c206f776e65722063616e2063616c6c207468697320707268696c652077616c6c657420697320696e207265636f766572790000000000004f7065726174696f6e2063616e206e6f7420626520706572666f726d656420776f78792066756e6374696f6e0000000000000000000000000000000000000000a165627a7a72305820e05b970657433124a989187ad543f1ce2f3a9447b537acbc66a75e507950e9420029";

    public static final String FUNC_CANCELRECOVERY = "cancelRecovery";

    public static final String FUNC_HASH2 = "hash2";

    public static final String FUNC_TRANSFERTO = "transferTo";

    public static final String FUNC_CHANGERECOVERYTIMEOUT = "changeRecoveryTimeout";

    public static final String FUNC_DOUBLEHASH = "doubleHash";

    public static final String FUNC_VIEWIDENTITYHASH = "viewIdentityHash";

    public static final String FUNC_SECONDSLEFTTORECOVER = "secondsLeftToRecover";

    public static final String FUNC_VERIFYIDENTITYDATA = "verifyIdentityData";

    public static final String FUNC_CHANGERECOVERYDELEGATE = "changeRecoveryDelegate";

    public static final String FUNC_MY_ADDRESS = "my_address";

    public static final String FUNC_FINISHRECOVERY = "finishRecovery";

    public static final String FUNC_HASH = "hash";

    public static final String FUNC_RECOVER = "recover";

    public static final Event RECOVERYINITIATED_EVENT = new Event("RecoveryInitiated",
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    public static final Event RECOVERYCOMPLETE_EVENT = new Event("RecoveryComplete",
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    public static final Event RECOVERYCANCELED_EVENT = new Event("RecoveryCanceled",
            Arrays.<TypeReference<?>>asList(new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}, new TypeReference<Address>(true) {}));
    ;

    @Deprecated
    protected MasterWallet(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected MasterWallet(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected MasterWallet(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected MasterWallet(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteCall<TransactionReceipt> cancelRecovery() {
        final Function function = new Function(
                FUNC_CANCELRECOVERY,
                Arrays.<Type>asList(),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<byte[]> hash2(String data) {
        final Function function = new Function(FUNC_HASH2,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(data)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bytes32>() {}));
        return executeRemoteCallSingleValueReturn(function, byte[].class);
    }

    public RemoteCall<TransactionReceipt> transferTo(String _to, BigInteger _amount) {
        final Function function = new Function(
                FUNC_TRANSFERTO,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_to),
                new org.web3j.abi.datatypes.generated.Uint256(_amount)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> changeRecoveryTimeout(BigInteger _timeout) {
        final Function function = new Function(
                FUNC_CHANGERECOVERYTIMEOUT,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(_timeout)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<byte[]> doubleHash(String data) {
        final Function function = new Function(FUNC_DOUBLEHASH,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(data)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bytes32>() {}));
        return executeRemoteCallSingleValueReturn(function, byte[].class);
    }

    public RemoteCall<byte[]> viewIdentityHash() {
        final Function function = new Function(FUNC_VIEWIDENTITYHASH,
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bytes32>() {}));
        return executeRemoteCallSingleValueReturn(function, byte[].class);
    }

    public RemoteCall<BigInteger> secondsLeftToRecover() {
        final Function function = new Function(FUNC_SECONDSLEFTTORECOVER,
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Boolean> verifyIdentityData(byte[] _identityData) {
        final Function function = new Function(FUNC_VERIFYIDENTITYDATA,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Bytes32(_identityData)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> changeRecoveryDelegate(String _recovery) {
        final Function function = new Function(
                FUNC_CHANGERECOVERYDELEGATE,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_recovery)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> my_address() {
        final Function function = new Function(FUNC_MY_ADDRESS,
                Arrays.<Type>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<TransactionReceipt> finishRecovery() {
        final Function function = new Function(
                FUNC_FINISHRECOVERY,
                Arrays.<Type>asList(),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<byte[]> hash(byte[] data) {
        final Function function = new Function(FUNC_HASH,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Bytes32(data)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bytes32>() {}));
        return executeRemoteCallSingleValueReturn(function, byte[].class);
    }

    public RemoteCall<TransactionReceipt> recover(byte[] _identityData, String _owner, String code) {
        final Function function = new Function(
                FUNC_RECOVER,
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Bytes32(_identityData),
                new org.web3j.abi.datatypes.Address(_owner),
                new org.web3j.abi.datatypes.Utf8String(code)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public List<RecoveryInitiatedEventResponse> getRecoveryInitiatedEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(RECOVERYINITIATED_EVENT, transactionReceipt);
        ArrayList<RecoveryInitiatedEventResponse> responses = new ArrayList<>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            RecoveryInitiatedEventResponse typedResponse = new RecoveryInitiatedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.wallet = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.owner = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<RecoveryInitiatedEventResponse> recoveryInitiatedEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new io.reactivex.functions.Function<Log, RecoveryInitiatedEventResponse>() {
            @Override
            public RecoveryInitiatedEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(RECOVERYINITIATED_EVENT, log);
                RecoveryInitiatedEventResponse typedResponse = new RecoveryInitiatedEventResponse();
                typedResponse.log = log;
                typedResponse.wallet = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.owner = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<RecoveryInitiatedEventResponse> recoveryInitiatedEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(RECOVERYINITIATED_EVENT));
        return recoveryInitiatedEventFlowable(filter);
    }

    public List<RecoveryCompleteEventResponse> getRecoveryCompleteEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(RECOVERYCOMPLETE_EVENT, transactionReceipt);
        ArrayList<RecoveryCompleteEventResponse> responses = new ArrayList<>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            RecoveryCompleteEventResponse typedResponse = new RecoveryCompleteEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.wallet = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.owner = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<RecoveryCompleteEventResponse> recoveryCompleteEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new io.reactivex.functions.Function<Log, RecoveryCompleteEventResponse>() {
            @Override
            public RecoveryCompleteEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(RECOVERYCOMPLETE_EVENT, log);
                RecoveryCompleteEventResponse typedResponse = new RecoveryCompleteEventResponse();
                typedResponse.log = log;
                typedResponse.wallet = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.owner = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<RecoveryCompleteEventResponse> recoveryCompleteEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(RECOVERYCOMPLETE_EVENT));
        return recoveryCompleteEventFlowable(filter);
    }

    public List<RecoveryCanceledEventResponse> getRecoveryCanceledEvents(TransactionReceipt transactionReceipt) {
        List<Contract.EventValuesWithLog> valueList = extractEventParametersWithLog(RECOVERYCANCELED_EVENT, transactionReceipt);
        ArrayList<RecoveryCanceledEventResponse> responses = new ArrayList<>(valueList.size());
        for (Contract.EventValuesWithLog eventValues : valueList) {
            RecoveryCanceledEventResponse typedResponse = new RecoveryCanceledEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.wallet = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.owner = (String) eventValues.getIndexedValues().get(1).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(2).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<RecoveryCanceledEventResponse> recoveryCanceledEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new io.reactivex.functions.Function<Log, RecoveryCanceledEventResponse>() {
            @Override
            public RecoveryCanceledEventResponse apply(Log log) {
                Contract.EventValuesWithLog eventValues = extractEventParametersWithLog(RECOVERYCANCELED_EVENT, log);
                RecoveryCanceledEventResponse typedResponse = new RecoveryCanceledEventResponse();
                typedResponse.log = log;
                typedResponse.wallet = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.owner = (String) eventValues.getIndexedValues().get(1).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(2).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<RecoveryCanceledEventResponse> recoveryCanceledEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(RECOVERYCANCELED_EVENT));
        return recoveryCanceledEventFlowable(filter);
    }

    @Deprecated
    public static MasterWallet load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new MasterWallet(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static MasterWallet load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new MasterWallet(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static MasterWallet load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new MasterWallet(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static MasterWallet load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new MasterWallet(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<MasterWallet> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider, String _owner, String _recovery, BigInteger _timeout, byte[] _identityHash) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_owner),
                new org.web3j.abi.datatypes.Address(_recovery),
                new org.web3j.abi.datatypes.generated.Uint256(_timeout),
                new org.web3j.abi.datatypes.generated.Bytes32(_identityHash)));
        return deployRemoteCall(MasterWallet.class, web3j, credentials, contractGasProvider, BINARY, encodedConstructor);
    }

    public static RemoteCall<MasterWallet> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider, String _owner, String _recovery, BigInteger _timeout, byte[] _identityHash) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_owner),
                new org.web3j.abi.datatypes.Address(_recovery),
                new org.web3j.abi.datatypes.generated.Uint256(_timeout),
                new org.web3j.abi.datatypes.generated.Bytes32(_identityHash)));
        return deployRemoteCall(MasterWallet.class, web3j, transactionManager, contractGasProvider, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<MasterWallet> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String _owner, String _recovery, BigInteger _timeout, byte[] _identityHash) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_owner),
                new org.web3j.abi.datatypes.Address(_recovery),
                new org.web3j.abi.datatypes.generated.Uint256(_timeout),
                new org.web3j.abi.datatypes.generated.Bytes32(_identityHash)));
        return deployRemoteCall(MasterWallet.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<MasterWallet> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String _owner, String _recovery, BigInteger _timeout, byte[] _identityHash) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(_owner),
                new org.web3j.abi.datatypes.Address(_recovery),
                new org.web3j.abi.datatypes.generated.Uint256(_timeout),
                new org.web3j.abi.datatypes.generated.Bytes32(_identityHash)));
        return deployRemoteCall(MasterWallet.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static class RecoveryInitiatedEventResponse {
        public Log log;

        public String wallet;

        public String owner;

        public String newOwner;
    }

    public static class RecoveryCompleteEventResponse {
        public Log log;

        public String wallet;

        public String owner;

        public String newOwner;
    }

    public static class RecoveryCanceledEventResponse {
        public Log log;

        public String wallet;

        public String owner;

        public String newOwner;
    }
}
