package com.masterwallet.oracle;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Hash;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.ManagedTransaction;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.utils.Numeric;

@Component
public class EtheriumService {

	private static final Logger LOG = LoggerFactory.getLogger(EtheriumService.class);

	private static final String PRIVATE_KEY = "7BE397832B4EAECAD17A69D8E249FD7E000D006BEA9D8B77E79F9A91D58C1575";
	private static final String CONTRACT_ADDRESS = "0x028309c4872abd639dfbf0114db53574b678f066";

	public static void main(String[] args) throws Exception {
		EtheriumService s = new EtheriumService();
		// s.sendPinCode("0x960a1dc8bb916d09df23e3af3092caed84dd232d", "1234");

		// http://51.15.199.52:8080/finishRecovery
		// ?ownerKey=0x4012626501ce6cc707c44167c5f8c6c05f5d4dcb0059af4656ad721b0f953436
		// &ownerAddress=0x0d8f2024e0e9caE557a908BA393ce745DDbFb564
		// &walletAddresss=0x2c0daf0b7a3aa7fe733ab3d9db34d183e8c02db1

		s.finishRecovery(
				"4012626501ce6cc707c44167c5f8c6c05f5d4dcb0059af4656ad721b0f953436",
				"0x0d8f2024e0e9caE557a908BA393ce745DDbFb564",
				"0x2c0daf0b7a3aa7fe733ab3d9db34d183e8c02db1");
	}

	public void test() {
		String expected = "0x64e604787cbf194841e7b68d7cd28786f6c9a0a3ab9f8b0a0e87cb4387ab0107";
		String pt = "123";

		byte[] hashLock = Hash.sha3(pt.getBytes());

		String hashHex = Numeric.toHexString(hashLock);
		System.out.println(expected);
		System.out.println(hashHex);
	}

	public void test2() {
		String expected = "0x7145c40868275c60d94b994eacfc6017ebfc0f2e9bf1e2f4a0500dd70695a78c";
		String pt = "12345";

		byte[] hashLock = Hash.sha3(Hash.sha3(pt.getBytes()));

		String hashHex = Numeric.toHexString(hashLock);
		System.out.println(expected);
		System.out.println(hashHex);
	}

	public String sendPinCode(String targetAddress, String pin) throws Exception {
		byte[] pkByes = Numeric.hexStringToByteArray(PRIVATE_KEY);
		ECKeyPair keyPair = ECKeyPair.create(pkByes);

		System.out.println("Success" + keyPair);


		Web3j web3 = Web3j.build(new HttpService("https://kovan.infura.io/902a99bd968d49988844c8573d45dcb7"));

		OracleService service = new OracleService(
				CONTRACT_ADDRESS,
				web3,
				Credentials.create(keyPair),
				new DefaultGasProvider());

		// Compute kekkak
		byte[] hashLock = Hash.sha3(pin.getBytes());

		LOG.info("Oracle hash: {}" + Numeric.toHexString(hashLock));

		TransactionReceipt rc = service.provide_lock(targetAddress, hashLock).send();
		String txId = rc.getTransactionHash();

		return txId;
	}

	public String finishRecovery(String ownerKey, String ownerAddress, String walletAddresss) throws Exception {
		byte[] pkByes = Numeric.hexStringToByteArray(ownerKey);
		ECKeyPair keyPair = ECKeyPair.create(pkByes);

		Web3j web3 = Web3j.build(new HttpService("https://kovan.infura.io/902a99bd968d49988844c8573d45dcb7"));
		MasterWallet mv = new MasterWallet(
				walletAddresss,
				web3,
				Credentials.create(keyPair),
				new StaticGasProvider(ManagedTransaction.GAS_PRICE, new BigInteger("45000")));
		return mv.finishRecovery().send().getTransactionHash();
	}
}
