package com.masterwallet.oracle;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import okhttp3.Authenticator;
import okhttp3.Call;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

@RestController
public class OracleWebService {

	private static final Logger LOG = LoggerFactory.getLogger(OracleWebService.class);

	@Autowired
	private EtheriumService eth;


	@RequestMapping(value = "finishRecovery",  method = RequestMethod.GET)
	public InvocationResponse finishRecovery(
			@RequestParam String ownerKey,
			@RequestParam String ownerAddress,
			@RequestParam String walletAddresss) throws Exception {

		String txId = eth.finishRecovery(ownerKey, ownerAddress, walletAddresss);
		InvocationResponse r = new InvocationResponse();
		r.setTxId(txId);

		return r;
	}

	@RequestMapping(value = "resetIdentity", method = RequestMethod.GET)
	public InvocationResponse resetIdentity(
			@RequestParam String phone,
			@RequestParam String address) throws Exception {
		// Generate new random sms code
		//
		Random random = new Random();
		String pin = String.valueOf(1000 + random.nextInt(8999));

		LOG.info("Pin code: {}" + pin);
		// Write data to blockchain
		//
		String txId = eth.sendPinCode(address, pin);
		LOG.info("Oracle written to Etherium in txId: {}" + txId);

		// trustAll();

		// Send pin in sms
		//
		String url = "https://gate.smsaero.ru/v2/" + "sms/send?numbers[]=" + phone + "&text=CODE+" + pin
				+ "&sign=SMS Aero&channel=DIRECT";

		OkHttpClient client = new OkHttpClient.Builder().readTimeout(1, TimeUnit.SECONDS)
				.authenticator(new Authenticator() {
					@Override
					public Request authenticate(Route route, Response response) throws IOException {
						String credential = Credentials.basic("akafakir@gmail.com", "0YBEW6jzInyp9TNBm63e2s8dYYC");
						return response.request().newBuilder().header("Authorization", credential).build();
					}
				}).build();

		Request request = new Request.Builder().url(url).build();

		Call call = client.newCall(request);
		Response response = call.execute();

		JSONObject json = new JSONObject(response.body().string());
		if (json.getBoolean("success")) {
			LOG.info("SMS Request ID: {}" + pin);
			json = json.getJSONArray("data").getJSONObject(0);
			String smsId = String.valueOf(json.getInt("id"));

			InvocationResponse rs = new InvocationResponse();
			rs.setSmsId(smsId);
			rs.setTxId(txId);

			return rs;

		} else {
			throw new RuntimeException("SMS Information error");
		}
	}

	public static void trustAll() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (GeneralSecurityException e) {
		}
	}
}
