import React from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableHighlight } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation'
// import {, createAppContainer} from 'react-navigation';
import CodeInput from 'react-native-code-input';

class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = { isLoading: true}
    }

    componentDidMount(){
        // return fetch('http://51.15.199.52:3000/balance')
        return fetch('https://10.20.3.68:3000/balance')
            .then((response) => {
                this.setState({
                    isLoading: false,
                    dataSource: response,
                }, function(){

                });

            })
            .catch((error) =>{
                console.error(error);
            });
    }

    render() {
        // RSA.generateKeys(4096).then(
        //     keys => {
        //         alert('4096 private:' + keys.private);
        //     }
        // );

        if(this.state.isLoading) {
            return (
                <View>Wait</View>
            )
        }

        return (
            <View style={styles.container}>
                <View style={styles.pocket}>
                    <Text style={styles.tokenAmount}>{this.state.dataSource} BTC</Text>
                    <Text style={styles.tokenAddBtn}>Пополнить кошелек</Text>
                </View>
                <View style={styles.payments}>
                    <Text style={styles.paymentsTitle}>Избранное:</Text>
                    <Text style={styles.paymentsTitle}>Платежи и переводы:</Text>
                    <Text style={styles.paymentsTitle}>Другое:</Text>
                </View>
            </View>
        );
    }
}

class RestoreScreen extends React.Component {
    render() {

        return  (
            <View>
                <Text style={styles.restoreTitle}>Восстановление кошелька</Text>

                <Text style={styles.subTitle}>Введите номер телефона</Text>
                <CodeInput
                    codeLength={11}
                    ref="codeInputRef1"
                    activeColor='rgba(49, 180, 4, 1)'
                    inactiveColor='rgba(49, 180, 4, 1.3)'
                    borderType={'underline'}
                    space={9}
                    size={30}
                    inputPosition='center'
                    onFulfill={(code) => this._onFulfill(code)}
                />
            </View>
        );
    }
}

class LoginScreen extends React.Component {
    render() {
        const {navigate} = this.props.navigation;

        // const web3 = new Web3(new Web3.providers.HttpProvider("https://kovan.infura.io/902a99bd968d49988844c8573d45dcb7"));
        // let res = web3.eth.accounts.create();
        // alert('res 2');
        fetch('http://10.20.3.68:5000/create').then(
            (response) => {
                alert(response)
            }
        );
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableHighlight style={styles.loginBtn}
                                    onPress={
                                        ()=>{
                                            navigate('Profile')
                                        }
                                    } underlayColor='#ff8c00'>
                    <Text style={styles.loginBtnTxt} >Войти</Text>
                </TouchableHighlight>

                <TouchableHighlight style={styles.restoreBtn}
                                    onPress={
                                        ()=>{
                                            navigate('Restore')
                                        }
                                    }
                                    underlayColor='#ff8c00'
                >
                    <Text style={styles.restoreBtnTxt} >Восстановить</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

class SettingsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{fontSize:24}}>Public key:</Text>
                <Text style={{width: '80%', marginTop: 50}}>
                    -----BEGIN PUBLIC KEY-----
                    MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe4eCZ0
                    FPqri0cb2JZfXJ/DgYSF6vUpwmJG8wVQZKjeGcjDOL5UlsuusFncCzWBQ7RKNUSesmQRMSGkVb1/
                    3j+skZ6UtW+5u09lHNsj6tQ51s1SPrCBkedbNf0Tp0GbMJDyR4e9T04ZZwIDAQAB
                    -----END PUBLIC KEY-----
                </Text>
            </View>
        );
    }
}

const TabNavigator = createBottomTabNavigator(
    {
        'Кошелек': HomeScreen,
        'Настройки': SettingsScreen,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                if (routeName === 'Кошелек') {
                    return <Image
                        source={require('./assets/pocket-fashion.png')}
                        style={{width: 28, height: 28}}
                    />
                }

                if (routeName === 'Настройки') {
                    return <Image
                        source={require('./assets/settings-gear.png')}
                        style={{width: 28, height: 28}}
                    />
                }

                // const { routeName } = navigation.state;
                // let IconComponent = Ionicons;
                // let iconName;
                // if (routeName === 'Home') {
                //     iconName = `ios-information-circle${focused ? '' : '-outline'}`;
                //     // Sometimes we want to add badges to some icons.
                //     // You can check the implementation below.
                //     IconComponent = HomeIconWithBadge;
                // } else if (routeName === 'Settings') {
                //     iconName = `ios-options`;
                // }
                //
                // // You can return any component that you like here!
                // return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
        },
    });

// export default createAppContainer(TabNavigator);
// export default LoginScreen

const MainNavigator = createStackNavigator({
    Home: {screen: LoginScreen},
    Profile: {screen: TabNavigator},
    Restore: {screen: RestoreScreen},
});

const App = createAppContainer(MainNavigator);

export default App;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 500
    },

    pocket: {
        width: '90%',
        height: 200,
        borderRadius: 15,
        marginTop: 100,
        backgroundColor: '#ff8c00',
        color: '#fff',
        justifyContent: 'center',
        textAlign: 'center',
    },

    tokenAmount: {
        fontSize: 32,
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold'
    },

    tokenAddBtn: {
        marginTop: 50,
        fontSize: 16,
        width: '80%',
        marginLeft: '10%',
        color: '#222',
        textAlign: 'center',
        backgroundColor: '#fff',
        height: 40,
        paddingTop: 9,
        justifyContent: 'center',
        borderRadius: 15,
        shadowOffset:{  width: 10,  height: 10,  },
        shadowColor: 'black',
        shadowOpacity: 1.0,
    },

    payments: {
        marginTop: 20,
        textAlign: 'left'
    },

    paymentsTitle: {
        fontSize: 26
    },

    loginBtn: {
        width: '70%',
        height: 40,
        backgroundColor: '#ff8c00',
        paddingTop: 10,
        borderRadius: 15
    },

    loginBtnTxt: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center',
    },

    restoreBtn: {
        width: '70%',
        height: 40,
        backgroundColor: '#ccc',
        paddingTop: 10,
        borderRadius: 15,
        marginTop: 20
    },

    restoreBtnTxt: {
        color: '#000',
        fontSize: 16,
        textAlign: 'center'
    },

    restoreTitle: {
        fontSize: 24,
        textAlign: 'center',
        marginTop: 20
    }
});


