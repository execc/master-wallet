import React, { Component, Fragment } from "react";
import MasterWallet from "./contracts/MasterWallet.json";
import getWeb3 from "./utils/getWeb3";

import "./App.css";

class App extends Component {
  state = { storageValue: 0, web3: null, accounts: null, pSeries: "", pNumber: "", pk: "", code: "", wallet: ""  };

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts }, this.runExample2);
      const context = this.runExample2.bind(this);

      setInterval(context, 1000);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  runExample2 = async () => {
   const { web3, accounts, wallet } = this.state;
    
      if (wallet) {
          // Get the contract instance.
      const contract = new web3.eth.Contract(
        MasterWallet.abi,
        wallet,
      );


     // Stores a given value, 5 by default.
    //await contract.methods.set(5).send({ from: accounts[0] });

    // Get the value from the contract to prove it worked.
    const response = await contract.methods.secondsLeftToRecover().call();

    // Update state with the result.
    this.setState({ storageValue: response });
      }
  };

  runExample = async () => {
    const { web3, accounts, pSeries, pNumber, pk, code, wallet } = this.state;

    if (wallet) {
          // Get the contract instance.
      const instance = new web3.eth.Contract(
        MasterWallet.abi,
        wallet,
      );
      const proof = web3.utils.sha3(pSeries + pNumber);
      console.log(pSeries + pNumber);
      console.log("code=" + code);

      await instance.methods.recover(
        proof, 
        pk, 
        code
      ).send({ from: accounts[0], value: web3.utils.toWei('0.001', 'ether') });

      // Stores a given value, 5 by default.
      //await contract.methods.set(5).send({ from: accounts[0] });

      // Get the value from the contract to prove it worked.
      const response = await instance.methods.secondsLeftToRecover().call();

      // Update state with the result.
      this.setState({ storageValue: response });
    }
  };

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    return (
      <div className="App">
        <h1>Оператор Авторизованного центра</h1>
        <p>Установлено соединение с узлом Etherium через Metamask.</p>

        <p>
          Адрес кошелька:
          <input type="text" value={this.state.wallet}  onChange={ e=> this.setState({wallet: e.target.value}) }/>
        </p>

        { (this.state.wallet && this.state.storageValue == 0) && this.renderRestore() }

        { (this.state.wallet && this.state.storageValue != 0 && this.state.storageValue < 100000  ) && <p>Доступ будет восстановлен через {this.state.storageValue} сек </p> }
        { (this.state.wallet && this.state.storageValue != 0 && this.state.storageValue > 100000  ) && <p>Пользователь должен подтвердать разблокировку своего кошелька </p> } 
      </div>
    );
  }

  renderRestore() {
    return (
      <Fragment>
        <h2>Введите данные для восстановления пароля</h2>
        <p>
          Серия паспорта: 
          <input type="text" value={this.state.pSeries} onChange={ e=> this.setState({pSeries: e.target.value}) }/>
        </p>
        <p>
          Номер паспорта: 
          <input type="text" value={this.state.pNumber} onChange={ e=> this.setState({pNumber: e.target.value}) }/>
        </p>
        <p>
          Проверочный код из СМС: 
          <input type="text" value={this.state.code} onChange={ e=> this.setState({code: e.target.value}) } />
        </p>
        <p>
          Новый публичный ключ: 
          <input type="text" value={this.state.pk}  onChange={ e=> this.setState({pk: e.target.value}) }/>
        </p>
        <p>
          <button onClick={this.runExample}>Восстановить доступ</button>
        </p>
      </Fragment>
    );
  }
}

export default App;
