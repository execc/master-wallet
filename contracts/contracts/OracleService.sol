pragma solidity ^0.4.25;

/// Oracle for providing second factor authentication
contract OracleService {
    
    /// Notifies mobile app that a new TFA may be created for a given target
    event TFACreated(
        address indexed target,
        bytes32 hashlock
    );
    
    constructor() public {
        owner = msg.sender;
    }
    
    /// Entity that deployed contract
    address owner;
    
    /// Locks data
    mapping (address => bytes32) lock_data;
     
    //// Provides new lock
    function provide_lock(address target, bytes32 hashlock) public {
         assert(msg.sender == owner);
         assert(target != address(0));
         assert(hashlock != 0);
         
         lock_data[target] = hashlock; /// TODO: Validate validity of hashlock by whitelisted signature
         
         emit TFACreated(target, hashlock);
     }
     
     function hash(string data) public pure returns (bytes32) {
         return keccak256(abi.encodePacked(data));
     }
     
     function verify(address addr, string data) public view returns (bool) {
         return lock_data[addr] == hash(data);
     }
}
