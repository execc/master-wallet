pragma solidity ^0.4.25;

import "./MasterWallet.sol";

/// Factory for creating Wallet Contracts
contract WalletFactory {
    
    event WalletCreated(
        address indexed walletAddress,
        address indexed walletOwner
    );
    
    function createWallet(address _owner, address _recovery, uint _timeout, bytes32 _identityHash) public returns (address) {
        address newContract = new MasterWallet(_owner, _recovery, _timeout, _identityHash);
        
        emit WalletCreated(_owner, newContract);
    }
    
         
     function doubleHash(string data) public pure returns (bytes32) {
         return keccak256(abi.encodePacked(keccak256(abi.encodePacked(data))));
     }
}
