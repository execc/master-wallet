pragma solidity ^0.4.25;

import "./OracleService.sol";

/// Wallet for which we can restore password
contract MasterWallet {
    
    /// Real account owner
    address owner;
    
    /// Recovery delegate
    address recovery; 
    
    /// Potential new address to recover
    address newOwner;
    
    /// Recovery timeout
    uint timeout;
    
    /// Recovery time
    uint recorveryTime;
    
    /// Hash of user identity
    bytes32 identityHash;
    
     function() payable public {
        // nothing to do
    }
    
    
    event RecoveryInitiated(
        address indexed wallet,
        address indexed owner,
        address indexed newOwner
    );
    
    event RecoveryComplete(
        address indexed wallet,
        address indexed owner,
        address indexed newOwner
    );
    
    event RecoveryCanceled(
        address indexed wallet,
        address indexed owner,
        address indexed newOwner
    );
    
    /// Creates a new managed MasterWallet
    constructor(address _owner, address _recovery, uint _timeout, bytes32 _identityHash) public {
        require(_owner != address(0));
        require(_recovery != address(0));
        require(_timeout > 0);
        require(_identityHash != 0);
        
        owner = _owner;
        recovery = _recovery;
        timeout = _timeout;
        identityHash = _identityHash;
    }
    
    /// Access control modifiersf
    modifier onlyOwner() {
        require(msg.sender == owner, "Only real owner can call this proxy function");
        _;
    }
    
    modifier onlyDelegate(){
        require(msg.sender == recovery, "Only delegate can make this call");
        _;
    }
    
    modifier onlyNewOwner() {
        require(msg.sender == newOwner, "Only real owner can make this call");
        _;
    }
    
    modifier notInRecovery() {
        require(newOwner == address(0), "Operation can not be performed while wallet is in recovery");
        _;
    }
    
    /// Transfers token to some other address
    function transferTo(address _to, uint _amount) public onlyOwner() notInRecovery() {
        _to.transfer(_amount);
    }
    
    /// Changes recovery delegate
    function changeRecoveryDelegate(address _recovery) public onlyOwner() notInRecovery() {
        require(_recovery != address(0));
        
        recovery = _recovery;
    }
    
    /// Changes recovery timeout
    function changeRecoveryTimeout(uint _timeout) public onlyOwner() notInRecovery() {
        timeout = _timeout;
    }
    
    /// Tries to recover account
    function recover(bytes32 _identityData, address _owner, string code) payable public onlyDelegate() notInRecovery() {
        require(msg.value >= 0.001 ether);
        // Verify that identity data match
        require(identityHash == hash(_identityData), "Identity hash lock fail");
        // TODO: Verity that signature of identity data is correct
        
        // Verify second factor with oracle service
        //
        OracleService service = OracleService(0x028309C4872aBD639dFbf0114DB53574b678F066);
        require(service.verify(_owner, code), "Oracle verificatitio failed");
        newOwner = _owner;
        recorveryTime = now;
        
        _owner.transfer(msg.value);
        
        emit RecoveryInitiated(address(this), owner, newOwner);
    }
    
    function finishRecovery() public onlyNewOwner() {
        require(newOwner != address(0));
        require(now > recorveryTime + timeout, "Can not make recovery ");
        
        emit RecoveryComplete(address(this), owner, newOwner);
        
        owner = newOwner;
        newOwner = address(0);
        recorveryTime = 0;
    }
    
    function cancelRecovery() public onlyOwner() {
        require(newOwner != address(0));
        require(now < recorveryTime + timeout, "Can not cancel recovery");
        
        emit RecoveryCanceled(address(this), owner, newOwner);
        
        newOwner = address(0);
        recorveryTime = 0;
    }
    
    /// Returns my address (for incoming payments)
    function my_address() public view returns (address) {
        return address(this);
    }
    
    function verifyIdentityData(bytes32 _identityData) public view returns (bool) {
        return identityHash == hash(_identityData);
    }
    
    function viewIdentityHash() public view returns (bytes32) {
        return identityHash;
    }
    
    function hash(bytes32 data) public pure returns (bytes32) {
         return keccak256(abi.encodePacked(data));
    }
     
    function hash2(string data) public pure returns(bytes32) {
         return keccak256(abi.encodePacked(data));
    }
     
    function doubleHash(string data) public pure returns (bytes32) {
         return keccak256(abi.encodePacked(keccak256(abi.encodePacked(data))));
    }
     
    function secondsLeftToRecover() public view returns (uint) {
         if (recorveryTime != 0) {
             return recorveryTime + timeout - now;
         }
         return 0;
    }
}
